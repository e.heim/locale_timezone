# == Class: sets on RedHat based systems the localtime  
class locale_timezone(
  $localtime = '' ) {
  exec { 'refresh_cache':
    command => "timedatectl set-timezone ${localtime}",
    path    => '/usr/local/bin/:/bin/',
    onlyif  => 'date | grep -cv UTC'
    }

  }
